package com.troncodroide.app.feature_characters.ui.characters

import android.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.core.domain.Pagination
import com.troncodroide.app.core.errors.Error
import com.troncodroide.app.core.extensions.networkCall
import com.troncodroide.app.domain.CharacterItemUI
import com.troncodroide.app.feature_characters.data.usecases.GetCharactersUsecase

class CharactersListViewModel(private val getCharactersUsecase: GetCharactersUsecase) :
    ViewModel() {

    private val pagination = Pagination()
    private val charactersMapped: MutableList<CharacterItemUI> = mutableListOf()

    private val viewState = MutableLiveData<CharactersViewState>()
    private val actions = MutableLiveData<Action>()
    private val errors = MutableLiveData<Error>()

    init {
        initGetCharacters()
    }

    fun getViewState(): LiveData<CharactersViewState> = viewState
    fun getActions(): LiveData<Action> = actions
    fun getErrorS(): LiveData<Error> = errors

    fun initGetCharacters() {
        viewState.value = CharactersViewState(
            initialLoading = true,
            characterItems = emptyList(),
            isRequestingNewPage = false
        )
        pagination.restart()
        getCharacters()
    }

    fun nextPage() {
        if (viewState.value?.isRequestingNewPage == false) {
            viewState.value = viewState.value?.copy(isRequestingNewPage = true)
            pagination.nextPage()
            getCharacters()
        }
    }

    private fun getCharacters() {
        getCharactersUsecase(pagination).map { list ->
            list.map { item ->
                CharacterItemUI(
                    id = item.id,
                    name = item.name,
                    images = item.images,
                    shortDescription = item.description,
                    backgroundTextsColor = Color.parseColor("#88000000"),
                    textsColor = Color.WHITE
                )
            }.apply { charactersMapped.addAll(this) }
            charactersMapped
        }.networkCall()
            .subscribe({
                viewState.postValue(
                    viewState.value?.copy(
                        initialLoading = false,
                        characterItems = it,
                        isRequestingNewPage = false
                    )
                )
            }, {
                errors.postValue(ErrorNetwork(it))
            })
    }
}

data class CharactersViewState(
    val initialLoading: Boolean,
    val characterItems: List<CharacterItemUI>,
    val isRequestingNewPage: Boolean
)

data class ErrorNetwork(override val throwable: Throwable) :
    Error(code = "10001", throwable = throwable, doneAction = null, retryAction = null)

data class OpenDetailCharacter(val characterID: Int) : Action.OpenAction(10001)