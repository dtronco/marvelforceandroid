package com.troncodroide.app.feature_characters.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.view.isVisible
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.domain.GroupUi
import com.troncodroide.app.feature_characters.databinding.GroupViewBinding

class CharacterGroupView(context: Context, attributeSet: AttributeSet) :
    LinearLayout(context, attributeSet) {

    init {
        orientation = VERTICAL
    }

    val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val binding = GroupViewBinding.inflate(inflater, this)

    fun setData(groupData: GroupUi, callback: (Action) -> Unit) {
        binding.groupTitle.text = groupData.name
        binding.groupItems.isVisible = groupData.available
        if (groupData.available) {
            binding.groupItems.removeAllViews()
            groupData.items.forEach {
                binding.groupItems.addView(GroupItemView(context)
                    .apply {
                        setData(it)
                        setActionCallback(callback)
                    })
            }
        }
    }
}