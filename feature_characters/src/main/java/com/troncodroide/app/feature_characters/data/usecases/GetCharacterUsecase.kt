package com.troncodroide.app.feature_characters.data.usecases

import com.troncodroide.app.data.repository.CharactersRepository
import com.troncodroide.app.domain.MarvelCharacter
import io.reactivex.rxjava3.core.Single

class GetCharacterUsecase(private val repository: CharactersRepository) {

    operator fun invoke(id: Int): Single<MarvelCharacter> {
        return repository.getCharacter(id)
    }
}