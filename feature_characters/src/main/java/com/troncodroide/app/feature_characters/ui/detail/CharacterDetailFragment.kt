package com.troncodroide.app.feature_characters.ui.detail

import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.R
import androidx.core.view.doOnLayout
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.snackbar.Snackbar
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.core.errors.Error
import com.troncodroide.app.core.extensions.loadAsset
import com.troncodroide.app.domain.CharacterDetailUI
import com.troncodroide.app.feature_characters.databinding.CharacterDetailFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit


class CharacterDetailFragment : Fragment() {

    companion object {
        const val CHARACTER_ID_EXTRA: String = "CharacterDetailFragment#CHARACTER_ID"
        const val NO_ID: Int = -1

        fun newInstance(characterId: Int): CharacterDetailFragment {
            val fragment = CharacterDetailFragment()
            val args = Bundle()
            args.putInt(CHARACTER_ID_EXTRA, characterId)
            fragment.arguments = args

            return fragment
        }
    }

    val viewModel: CharacterDetailViewModel by viewModel()
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<NestedScrollView>
    private lateinit var viewBinding: CharacterDetailFragmentBinding
    private var actionBarSize: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition(500, TimeUnit.MILLISECONDS)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = CharacterDetailFragmentBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
        getThemeData()
        initBottomSheet()
        handleArguments()
    }

    private fun getThemeData() {
        val tv = TypedValue()
        if (requireContext().theme.resolveAttribute(R.attr.actionBarSize, tv, true)) {
            actionBarSize = TypedValue.complexToDimensionPixelSize(
                tv.data,
                resources.displayMetrics
            )
        }
    }

    private fun initBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(viewBinding.characterDetailBottomsheet)
        bottomSheetBehavior.isHideable = false
        bottomSheetBehavior.peekHeight = 200

    }

    private fun handleArguments() {
        arguments?.let {
            val characterId = it.getInt(CHARACTER_ID_EXTRA, NO_ID)
            viewModel.setCharacter(characterId)
        }
    }

    private fun initObservers() {
        viewModel.getViewState().observe(viewLifecycleOwner) { loadState(it) }
        viewModel.getActions().observe(viewLifecycleOwner) { doAction(it) }
        viewModel.getErrorS().observe(viewLifecycleOwner) { showError(it) }
    }

    private fun loadState(detailState: CharacterViewState?) {
        detailState?.let {
            loading(it.initialLoading)
            state(it.characterDetail)
        }
    }

    private fun state(characterDetail: CharacterDetailUI?) {
        characterDetail?.images?.let { viewBinding.characterItemImage.loadAsset(it) }
        characterDetail?.let {
            viewBinding.characterDetailBottom.setData(it, ::doAction)
        }
        viewBinding.characterDetailBottom.doOnLayout {
            bottomSheetBehavior.peekHeight =
                actionBarSize + viewBinding.characterDetailBottom.getTitleViewHeight()
        }
    }

    private fun loading(initialLoading: Boolean) {

    }

    private fun doAction(action: Action?) {
        action?.let {
            when (action) {
                is Action.OpenUrlAction -> {
                    //This url is not on this scope, I should open a new Framgent with the comic/series... api call
                    Toast.makeText(
                        requireContext(),
                        "Not on scope, add new task to create the item detail fragment",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                else -> {
                    // do other actions with base action digester
                }
            }
        }
    }

    private fun showError(error: Error) {
        error.log()
        Snackbar.make(viewBinding.root, error.code, Snackbar.LENGTH_INDEFINITE).setBackgroundTint(
            Color.RED
        ).show()
    }

}