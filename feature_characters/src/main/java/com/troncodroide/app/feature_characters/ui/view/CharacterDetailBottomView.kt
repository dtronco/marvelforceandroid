package com.troncodroide.app.feature_characters.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.core.extensions.dp
import com.troncodroide.app.domain.CharacterDetailUI
import com.troncodroide.app.domain.GroupUi
import com.troncodroide.app.feature_characters.databinding.CharacterDetailBottomViewBinding
import com.troncodroide.app.feature_characters.databinding.GroupViewBinding


class CharacterDetailBottomView(context: Context, attributeSet: AttributeSet) :
    LinearLayout(context, attributeSet) {

    val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    val binding = CharacterDetailBottomViewBinding.inflate(inflater, this)

    init {
        orientation = VERTICAL
    }

    fun setData(character: CharacterDetailUI, actionCallback : (Action) -> Unit) {
        binding.characterItemText.text = character.name
        binding.characterItemDescription.text = character.description
        binding.characterItemComicsContainer.setData(character.comics, actionCallback)
        binding.characterItemSeriesContainer.setData(character.series,actionCallback)
        binding.characterItemStoriesContainer.setData(character.stories, actionCallback)
    }

    fun getTitleViewHeight(): Int = binding.characterItemText.measuredHeight

}

