package com.troncodroide.app.feature_characters.data.datasource

import com.troncodroide.app.core.domain.Pagination
import com.troncodroide.app.data.datasource.CharactersCacheDataSource
import com.troncodroide.app.domain.MarvelCharacter
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

class CharactersMemoryCacheDataSource : CharactersCacheDataSource {

    private val cache: MutableMap<Int, List<MarvelCharacter>> = mutableMapOf()

    override fun hasCharacter(id: Int): Single<Boolean> {
        cache.values.forEach {
            val finded = it.find { it.id == id }
            if (finded != null) {
                return Single.just(true)
            }
        }
        return Single.just(false)

    }

    override fun getCharacter(id: Int): Single<MarvelCharacter> {
        cache.values.forEach {
            val finded = it.find { it.id == id }
            if (finded != null) {
                return Single.just(finded)
            }
        }
        return Single.error(Exception("Not found on cache"))
    }

    override fun hasData(pagination: Pagination): Single<Boolean> {
        return Single.just(cache.contains(pagination.currentPage))
    }

    override fun getData(pagination: Pagination): Single<List<MarvelCharacter>> {
        return Single.just(cache.get(key = pagination.currentPage).orEmpty())
    }

    override fun storeData(pagination: Pagination, data: List<MarvelCharacter>): Completable {
        return Completable.fromCallable {
            cache.put(pagination.currentPage, data)
        }
    }

    override fun clearData(pagination: Pagination): Completable {
        return Completable.fromCallable {
            cache.remove(pagination.currentPage)
        }
    }

    override fun clearAll(): Completable {
        return Completable.fromCallable {
            cache.clear()
        }
    }
}