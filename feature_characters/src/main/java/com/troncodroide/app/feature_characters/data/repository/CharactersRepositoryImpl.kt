package com.troncodroide.app.feature_characters.data.repository

import com.troncodroide.app.core.domain.Pagination
import com.troncodroide.app.data.datasource.CharactersCacheDataSource
import com.troncodroide.app.data.datasource.CharactersNetworkDataSource
import com.troncodroide.app.data.datasource.CharactersPersistenceDataSource
import com.troncodroide.app.data.repository.CharactersRepository
import com.troncodroide.app.domain.MarvelCharacter
import io.reactivex.rxjava3.core.Single

class CharactersRepositoryImpl(
    val cache: CharactersCacheDataSource,
    val network: CharactersNetworkDataSource

) : CharactersRepository {

    override fun getCharacters(pagination: Pagination): Single<List<MarvelCharacter>> {
        return cache.hasData(pagination).flatMap { isOnCache ->
            if (isOnCache) cache.getData(pagination) else network.getCharacters(pagination).doOnSuccess { cache.storeData(pagination, it) }
        }
    }

    override fun getCharacter(id: Int): Single<MarvelCharacter> {
        return cache.hasCharacter(id).flatMap { isOnCache ->
            if (isOnCache) cache.getCharacter(id) else network.getCharacter(id)
        }
    }
}