package com.troncodroide.app.feature_characters.ui.mapper

import android.content.res.Resources
import com.troncodroide.app.domain.*
import com.troncodroide.app.feature_characters.R

object DataToUIMapper {

    fun mapToUi(item: MarvelCharacter, resources: Resources): CharacterDetailUI {
        return item.mapToDetailUI(resources)
    }

    fun MarvelCharacter.mapToDetailUI(resources: Resources): CharacterDetailUI {
        val comicLabel =
            if (comics.available) R.string.character_detail_labels_comics else R.string.character_detail_labels_no_comics
        val seriesLabel =
            if (series.available) R.string.character_detail_labels_series else R.string.character_detail_labels_no_series
        val storiesLabel =
            if (stories.available) R.string.character_detail_labels_stories else R.string.character_detail_labels_no_stories
        return CharacterDetailUI(
            id = id,
            name = name,
            images = images,
            description = description,
            comics = comics.toGroupUI(resources.getString(comicLabel)),
            stories = stories.toGroupUI(resources.getString(storiesLabel)),
            series = series.toGroupUI(resources.getString(seriesLabel))
        )
    }

    private fun GroupItem.toGroupUI(label: String): GroupUi {
        return GroupUi(available, label, items.map { ItemUi(it.name, it.url) })
    }
}