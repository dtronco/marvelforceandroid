package com.troncodroide.app.feature_characters.ui.characters

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.core.actions.ActionListener
import com.troncodroide.app.core.errors.Error
import com.troncodroide.app.domain.CharacterItemUI
import com.troncodroide.app.feature_characters.databinding.CharactersFragmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class CharactersListFragment : Fragment() {

    companion object {
        fun newInstance() = CharactersListFragment()
    }

    private var actionListener: ActionListener? = null
    private val viewModel: CharactersListViewModel by viewModel()
    lateinit var viewBinding: CharactersFragmentBinding

    override fun onAttach(context: Context) {
        super.onAttach(context)
        actionListener = (context as? ActionListener)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = CharactersFragmentBinding.inflate(inflater)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObservers()
    }

    private fun initObservers() {
        viewModel.getViewState().observe(viewLifecycleOwner) { loadState(it) }
        viewModel.getActions().observe(viewLifecycleOwner) { doAction(it) }
        viewModel.getErrorS().observe(viewLifecycleOwner) { showError(it) }
    }

    private fun doAction(action: Action?) {
        action?.let {
            when (action) {
                is OpenDetailCharacter -> {
                    Toast.makeText(
                        requireContext(),
                        "OpenDetail ${action.characterID}",
                        Toast.LENGTH_SHORT
                    ).show()
                    actionListener?.onAction(action)
                }
                else -> {
                    // do other actions with base action digester
                }
            }
        }
    }

    private fun showError(error: Error) {
        Snackbar.make(viewBinding.root, error.code, Snackbar.LENGTH_INDEFINITE).setBackgroundTint(
            Color.RED
        ).show()
    }

    private fun loadState(state: CharactersViewState?) {
        state?.initialLoading?.let { isLoading(it) }
        state?.characterItems?.let { loadData(it) }
        state?.isRequestingNewPage?.let { isRequestingNewPage(it) }
    }

    private fun isRequestingNewPage(loading: Boolean) {
        viewBinding.loadingNewPage.isVisible = loading
    }

    private fun isLoading(loading: Boolean) {
        viewBinding.loading.isVisible = loading
    }

    private fun loadData(it: List<CharacterItemUI>) {
        viewBinding.itemlist.setData(it)
        viewBinding.itemlist.setOnNewPageNeededListener { nextPage() }
        viewBinding.itemlist.apply {
            setData(it)
            setOnNewPageNeededListener { nextPage() }
            setOnItemClickListener { itemClick(it) }
        }
    }

    private fun itemClick(characterItemUI: CharacterItemUI) {
        doAction(OpenDetailCharacter(characterID = characterItemUI.id))
    }

    private fun nextPage() {
        Log.i("MarvelLegacy", "Click on search button")
        viewModel.nextPage()
    }

}