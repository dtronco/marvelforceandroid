package com.troncodroide.app.feature_characters.ui.detail

import android.content.res.Resources
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.core.errors.Error
import com.troncodroide.app.core.extensions.networkCall
import com.troncodroide.app.domain.CharacterDetailUI
import com.troncodroide.app.feature_characters.data.usecases.GetCharacterUsecase
import com.troncodroide.app.feature_characters.ui.mapper.DataToUIMapper

class CharacterDetailViewModel(
    private val getCharacterUsecase: GetCharacterUsecase,
    private val resources: Resources
) : ViewModel() {


    private val viewState = MutableLiveData<CharacterViewState>()
    private val actions = MutableLiveData<Action>()
    private val errors = MutableLiveData<Error>()

    init {
        initGetCharacter()
    }

    fun getViewState(): LiveData<CharacterViewState> = viewState
    fun getActions(): LiveData<Action> = actions
    fun getErrorS(): LiveData<Error> = errors

    fun initGetCharacter() {
        viewState.value = CharacterViewState(initialLoading = true, null)
    }

    fun setCharacter(id: Int) {
        if (id == -1) {
            errors.postValue(ErrorNetwork(Exception()))
        } else
            getCharacterUsecase(id).map { DataToUIMapper.mapToUi(it, resources) }
                .networkCall()
                .subscribe({
                    viewState.postValue(
                        viewState.value?.copy(
                            initialLoading = false,
                            characterDetail = it,
                        )
                    )
                }, {
                    errors.postValue(ErrorNetwork(it))
                })
    }


}

data class CharacterViewState(
    val initialLoading: Boolean = true,
    val characterDetail: CharacterDetailUI? = null
)

data class ErrorNetwork(override val throwable: Throwable) :
    Error(code = "10002", throwable = throwable, doneAction = null, retryAction = null)

