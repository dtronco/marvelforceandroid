package com.troncodroide.app.feature_characters.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.domain.Item
import com.troncodroide.app.domain.ItemUi
import com.troncodroide.app.feature_characters.databinding.GroupItemViewBinding


class GroupItemView(context: Context, attributeSet: AttributeSet? = null) :
    ConstraintLayout(context, attributeSet) {
    private var itemClickListener: (Action) -> Unit = {}

    private val inflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    private val binding = GroupItemViewBinding.inflate(inflater, this)

    fun setData(item: ItemUi) {
        binding.groupTitle.text = item.name
        binding.root.setOnClickListener {
            itemClickListener(Action.OpenUrlAction(10004, item.url))
        }
    }

    fun setActionCallback(callbacK: (Action) -> Unit) {
        itemClickListener = callbacK
    }


}