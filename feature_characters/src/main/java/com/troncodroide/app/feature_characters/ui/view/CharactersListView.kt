package com.troncodroide.app.feature_characters.ui.view

import android.content.Context
import android.content.res.Configuration
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.troncodroide.app.core.extensions.loadAsset
import com.troncodroide.app.domain.CharacterItemUI
import com.troncodroide.app.feature_characters.databinding.CharactersItemViewBinding


class CharactersListView(context: Context, attributeSet: AttributeSet) :
    RecyclerView(context, attributeSet) {
    private val scrollListener = ScrollListener({})
    private var itemClickListener: (CharacterItemUI) -> Unit = {}

    init {
        val gridColums =
            if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                4
            } else {
                2
            }
        layoutManager = GridLayoutManager(context, gridColums, GridLayoutManager.VERTICAL, false)
        adapter = CharactersAdapter { item ->
            itemClickListener(item)
        }
        val snapHelper = LinearSnapHelper()
        snapHelper.attachToRecyclerView(this)
        addEndlessLoading()
    }

    private fun addEndlessLoading() {
        addOnScrollListener(scrollListener)
    }

    fun setData(characters: List<CharacterItemUI>) {
        (adapter as CharactersAdapter).setData(characters)
    }

    fun setOnNewPageNeededListener(callbacK: () -> Unit) {
        scrollListener.callbacK = callbacK
    }

    fun setOnItemClickListener(callbacK: (CharacterItemUI) -> Unit) {
        itemClickListener = callbacK
    }

    class ScrollListener(var callbacK: () -> Unit) : OnScrollListener() {
        private var previousTotal = 0
        private var loading = true
        private val visibleThreshold = 5
        var firstVisibleItem = 0
        var visibleItemCount: Int = 0
        var totalItemCount: Int = 0

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val layoutManager = recyclerView.layoutManager as? LinearLayoutManager
            visibleItemCount = recyclerView.getChildCount()
            totalItemCount = layoutManager?.getItemCount() ?: 0
            firstVisibleItem = layoutManager?.findFirstVisibleItemPosition() ?: 0

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false
                    previousTotal = totalItemCount;
                }
            }
            if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)
            ) {
                // End has been reached
                loading = true
                callbacK()
            }
        }
    }
}

class CharactersAdapter(itemClick: (CharacterItemUI) -> Unit) :
    RecyclerView.Adapter<CharactersAdapter.ViewHolder>() {

    private val itemClick: (item: CharacterItemUI) -> Unit = {
        itemClick(it)
    }
    private val dataSet: MutableList<CharacterItemUI> = mutableListOf()

    init {
        setHasStableIds(true)
    }

    override fun getItemId(position: Int): Long {
        return position.item().id.toLong()
    }

    private fun Int.item(): CharacterItemUI = dataSet[this]

    class ViewHolder(val binding: CharactersItemViewBinding) : RecyclerView.ViewHolder(binding.root)

    fun setData(characters: List<CharacterItemUI>) {
        dataSet.clear()
        dataSet.addAll(characters)
        notifyItemRangeChanged(0, dataSet.size)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val viewBinding = CharactersItemViewBinding.inflate(
            LayoutInflater.from(viewGroup.context),
            viewGroup,
            false
        )
        return ViewHolder(viewBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = position.item()
        viewHolder.binding.apply {
            main.setOnClickListener {
                itemClick(item)
            }
            characterItemText.text = item.name
            characterItemImage.loadAsset(item.images)
            characterItemTextsBg.setBackgroundColor(item.backgroundTextsColor)
            characterItemText.setTextColor(item.textsColor)
            characterItemDescription.setTextColor(item.textsColor)
            characterItemDescription.text = item.shortDescription
        }
    }

    override fun getItemCount() = dataSet.size
}