import com.troncodroid.gradle.Deps

plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    compileSdk = com.troncodroid.gradle.ConfigData.compileSdkVersion

    defaultConfig {
        minSdk = com.troncodroid.gradle.ConfigData.minSdkVersion
        targetSdk = com.troncodroid.gradle.ConfigData.targetSdkVersion

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = com.troncodroid.gradle.ConfigData.javaVersion
        targetCompatibility = com.troncodroid.gradle.ConfigData.javaVersion
    }
    kotlinOptions {
        jvmTarget = com.troncodroid.gradle.ConfigData.jvmTarget
    }
    viewBinding {
        isEnabled = true
    }
}

dependencies {

    implementation(project(":core"))
    implementation(project(":domain"))
    implementation(project(":data"))
    implementation(Deps.coreKtx)
    implementation(Deps.appCompat)
    implementation(Deps.materialDesign)
    implementation(Deps.rxjava)
    implementation(Deps.rxandroid)
    implementation(Deps.koin)
    testImplementation(Deps.junit)
    androidTestImplementation(Deps.androidxJunit)
    androidTestImplementation(Deps.expressoCore)
}