package com.troncodroid.gradle

object Versions {
    const val gradlePlugin = "4.2.1"
    const val kotlin = "1.5.0"
    const val timber = "4.7.1"
    const val coreKtx = "1.7.0"
    const val appCompat = "1.4.1"
    const val material = "1.5.0"
    const val constraintLayout = "2.1.3"
    const val jUnit = "4.13.2"
    const val androidJunit = "1.1.3"
    const val lifecycle = "2.4.1"
    const val expressoCore = "3.4.0"
    const val rxjava = "3.0.0"
    const val retrofit = "2.9.0"
    const val okhttp3 = "4.9.2"
    const val koinVersion = "3.1.6"
    const val picasso = "2.71828"
    const val palette = "1.0.0-alpha1"
}


object BuildPlugins {
    val android by lazy { "com.android.tools.build:gradle:${Versions.gradlePlugin}" }
    val kotlin by lazy { "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}" }
}


object Deps {
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
    const val coreKtx = "androidx.core:core-ktx:${Versions.coreKtx}"
    const val timber = "com.jakewharton.timber:timber:${Versions.timber}"
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    const val materialDesign = "com.google.android.material:material:${Versions.material}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
    const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.lifecycle}"
    const val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}"

    // android support
    const val palette = "androidx.palette:palette:${Versions.palette}"

    // dependency injector
    const val koin = "io.insert-koin:koin-android:${Versions.koinVersion}"

    // RxJava
    const val rxjava = "io.reactivex.rxjava3:rxjava:${Versions.rxjava}"
    const val rxandroid = "io.reactivex.rxjava3:rxandroid:${Versions.rxjava}"

    // Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
    const val interceptor = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp3}"
    const val converterGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
    const val adapter = "com.squareup.retrofit2:adapter-rxjava3:${Versions.retrofit}"

    // Image loader
    const val picasso = "com.squareup.picasso:picasso:${Versions.picasso}"


    /* Test */
    const val junit = "junit:junit:${Versions.jUnit}"
    const val androidxJunit = "androidx.test.ext:junit:${Versions.androidJunit}"
    const val expressoCore = "androidx.test.espresso:espresso-core:${Versions.expressoCore}"
}

object ConfigData {
    const val compileSdkVersion = 31
    const val buildToolsVersion = "30.0.3"
    const val minSdkVersion = 21
    const val targetSdkVersion = 31
    const val versionCode = 1
    const val versionName = "1.0"
    const val jvmTarget = "1.8"
    val javaVersion = org.gradle.api.JavaVersion.VERSION_1_8
}