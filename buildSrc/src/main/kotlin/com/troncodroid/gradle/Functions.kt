package com.troncodroid.gradle

import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit

object Functions {
    fun getVersionCode(): Int? {
        return "git rev-list --count HEAD".runCommand(File("./"))?.toInt()
    }
}


private fun String.runCommand(workingDir: File): String? {
    try {
        val parts = this.split("\\s".toRegex())
        val proc = ProcessBuilder(*parts.toTypedArray())
            .directory(workingDir)
            .redirectOutput(ProcessBuilder.Redirect.PIPE)
            .redirectError(ProcessBuilder.Redirect.PIPE)
            .start()

        proc.waitFor(60, TimeUnit.MINUTES)
        return proc.inputStream.bufferedReader().readText().trim()
    } catch (e: IOException) {
        e.printStackTrace()
        return null
    }
}