package com.troncodroide.app.core.actions

sealed class Action(open val id: Int) {

    open class NavigationAction(override val id: Int): Action(id)

    open class OpenAction(override val id: Int): Action(id)

    open class OpenUrlAction(override val id: Int,val url:String): Action(id)

    open class SendAction(override val id: Int): Action(id)

    open class ShareAction(override val id: Int): Action(id)

    open class IntentAction(override val id: Int): Action(id)

}