package com.troncodroide.app.core.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText

class AppEditTextView: AppCompatEditText{
    constructor(context: Context) : super(context)
    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet)
    constructor(context: Context, attributeSet: AttributeSet?, theme: Int) : super(
        context,
        attributeSet,
        theme
    )
}