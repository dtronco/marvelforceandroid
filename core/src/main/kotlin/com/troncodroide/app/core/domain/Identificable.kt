package com.troncodroide.app.core.domain

interface Identificable {
    val id: Int
}