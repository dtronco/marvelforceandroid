package com.troncodroide.app.core.data

import com.troncodroide.app.core.actions.Action
import io.reactivex.rxjava3.core.Completable

interface ActionConsumer {

    fun digestAction(action: Action): Completable
}