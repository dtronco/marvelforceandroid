package com.troncodroide.app.core.extensions

import android.content.res.Resources
import android.util.Log
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import com.troncodroide.app.core.domain.Asset
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.Exception
import kotlin.math.roundToInt

val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()

val Float.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density).roundToInt()

fun ImageView.loadAsset(asset: Asset, loaded: () -> Unit = {}) {
    when (asset) {
        is Asset.AssetUri -> Picasso.get().load(asset.url).into(this, object: Callback {
            override fun onSuccess() {
                loaded()
            }

            override fun onError(e: Exception?) {
                Log.e("Picasso", "Error getting image: ${asset.url}", e)
            }
        })
        is Asset.AssetRes -> setImageResource(asset.resID).apply { loaded() }
        else -> {}
    }
}

inline fun <reified T : Enum<T>> enumContains(name: String?): Boolean {
    return enumValues<T>().any { it.name == name}
}

inline fun <reified T : Any> Single<T>.networkCall(): Single<T> {
    return this.workOnBackground().resultOnMain()
}

fun Completable.resultOnMain(): Completable {
    return observeOn(AndroidSchedulers.mainThread())
}

inline fun <reified T: Any> Single<T>.resultOnMain(): Single<T> {
    return observeOn(AndroidSchedulers.mainThread())
}

inline fun <reified T: Any> Flowable<T>.resultOnMain(): Flowable<T> {
    return observeOn(AndroidSchedulers.mainThread())
}

inline fun <reified T: Any> Maybe<T>.resultOnMain(): Maybe<T> {
    return observeOn(AndroidSchedulers.mainThread())
}

fun Completable.resultOnBackground(): Completable {
    return observeOn(Schedulers.io())
}

inline fun <reified T: Any> Single<T>.resultOnBackground(): Single<T> {
    return observeOn(Schedulers.io())
}

inline fun <reified T: Any> Flowable<T>.resultOnBackground(): Flowable<T> {
    return observeOn(Schedulers.io())
}

fun Completable.resultOnComputation(): Completable {
    return observeOn(Schedulers.computation())
}

inline fun <reified T: Any> Single<T>.resultOnComputation(): Single<T> {
    return observeOn(Schedulers.computation())
}


inline fun <reified T: Any> Flowable<T>.resultOnComputation(): Flowable<T> {
    return observeOn(Schedulers.computation())
}


fun Completable.workOnMain(): Completable {
    return subscribeOn(AndroidSchedulers.mainThread())
}

inline fun <reified T: Any> Single<T>.workOnMain(): Single<T> {
    return subscribeOn(AndroidSchedulers.mainThread())
}

inline fun <reified T: Any> Flowable<T>.workOnMain(): Flowable<T> {
    return subscribeOn(AndroidSchedulers.mainThread())
}

fun Completable.workOnRealm(): Completable {
    return subscribeOn(Schedulers.computation())
}

inline fun <reified T: Any> Single<T>.workOnRealm(): Single<T> {
    return subscribeOn(Schedulers.computation())
}

inline fun <reified T: Any> Flowable<T>.workOnRealm(): Flowable<T> {
    return subscribeOn(Schedulers.computation())
}

fun Completable.workOnBackground(): Completable {
    return subscribeOn(Schedulers.io())
}

inline fun <reified T: Any> Single<T>.workOnBackground(): Single<T> {
    return subscribeOn(Schedulers.io())
}


inline fun <reified T: Any> Flowable<T>.workOnBackground(): Flowable<T> {
    return subscribeOn(Schedulers.io())
}


inline fun <reified T: Any> Maybe<T>.workOnBackground(): Maybe<T> {
    return subscribeOn(Schedulers.io())
}

fun Completable.workOnNewThread(): Completable {
    return subscribeOn(Schedulers.newThread())
}

inline fun <reified T: Any> Single<T>.workOnNewThread(): Single<T> {
    return subscribeOn(Schedulers.newThread())
}


inline fun <reified T: Any> Flowable<T>.workOnNewThread(): Flowable<T> {
    return subscribeOn(Schedulers.newThread())
}

fun Completable.workOnComputation(): Completable {
    return subscribeOn(Schedulers.computation())
}

inline fun <reified T: Any> Single<T>.workOnComputation(): Single<T> {
    return subscribeOn(Schedulers.computation())
}


inline fun <reified T: Any> Flowable<T>.workOnComputation(): Flowable<T> {
    return subscribeOn(Schedulers.computation())
}