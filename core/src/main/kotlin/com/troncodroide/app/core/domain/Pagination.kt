package com.troncodroide.app.core.domain

data class Pagination(private var page: Int = 0, val itemsPerPage: Int = 30) {


    val offset: Int
        get() {
            return page * itemsPerPage
        }

    val currentPage: Int = page

    fun nextPage() {
        page++
    }

    fun restart() {
        page = 0
    }
}