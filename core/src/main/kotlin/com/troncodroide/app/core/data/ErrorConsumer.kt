package com.troncodroide.app.core.data

import com.troncodroide.app.core.errors.Error
import io.reactivex.rxjava3.core.Completable

interface ErrorConsumer {

    fun digestError(error: Error): Completable
}