package com.troncodroide.app.core.domain

import androidx.annotation.DrawableRes

sealed class Asset {
    class AssetUri(val url: String) : Asset()
    class AssetColorInt(val color: Int) : Asset()
    class AssetColorsInt(val colors: IntArray) : Asset()
    class AssetRes(@DrawableRes val resID: Int) : Asset()
    object NoAsset :Asset()
    companion object {
        fun fromUrl(url: String): Asset = AssetUri(url)

        fun fromRes(@DrawableRes resID: Int): Asset = AssetRes(resID)
    }
}