package com.troncodroide.app.core.actions

interface ActionListener {

    fun onAction(action: Action)
}