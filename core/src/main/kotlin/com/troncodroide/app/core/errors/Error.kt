package com.troncodroide.app.core.errors

import android.util.Log
import com.troncodroide.app.core.actions.Action

open class Error(
    open val throwable: Throwable? = null,
    open val code: String,
    open val retryAction: Action? = null,
    open val doneAction: Action? = null
) {
    fun log() {
        Log.e("Error", code, throwable)
    }
}