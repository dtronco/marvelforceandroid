package com.troncodroid.app.marvel

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.troncodroide.app.core.actions.Action
import com.troncodroide.app.core.actions.ActionListener
import com.troncodroide.app.feature_characters.ui.characters.CharactersListFragment
import com.troncodroide.app.feature_characters.ui.characters.OpenDetailCharacter
import com.troncodroide.app.feature_characters.ui.detail.CharacterDetailFragment

class MainActivity : AppCompatActivity(), ActionListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, CharactersListFragment.newInstance())
                .commitNow()
        }
        Log.i("MarvelLegacy", "Start activity")
    }

    override fun onAction(action: Action) {
        if (action is OpenDetailCharacter) {
            supportFragmentManager.beginTransaction()
                .setCustomAnimations(
                    com.troncodroide.app.core.R.anim.enter,
                    com.troncodroide.app.core.R.anim.exit
                )
                .replace(R.id.container, CharacterDetailFragment.newInstance(action.characterID))
                .addToBackStack("/")
                .commit()
        }
    }
}