package com.troncodroide.app.domain

import androidx.annotation.ColorInt
import com.troncodroide.app.core.domain.Asset

data class MarvelCharacter(
    val id: Int,
    val name: String,
    val images: Asset,
    val description: String,
    val comics: GroupItem,
    val stories: GroupItem,
    val series: GroupItem
)

data class GroupItem(val available: Boolean, val items: List<Item>)
data class Item(val name: String, val url: String)

data class CharacterDetailUI(
    val id: Int,
    val name: String,
    val images: Asset,
    val description: String,
    val comics: GroupUi,
    val stories: GroupUi,
    val series: GroupUi
)

data class GroupUi(val available: Boolean, val name: String, val items: List<ItemUi>)
data class ItemUi(val name: String, val url: String)

data class CharacterItemUI(
    val id: Int,
    val name: String,
    val images: Asset,
    val shortDescription: String,
    @ColorInt val backgroundTextsColor: Int,
    @ColorInt val textsColor: Int
)