package com.troncodroide.app.networkretrofit.data.datasource

import com.troncodroide.app.core.domain.Asset
import com.troncodroide.app.domain.GroupItem
import com.troncodroide.app.domain.Item
import com.troncodroide.app.domain.MarvelCharacter
import com.troncodroide.app.networkretrofit.domain.*

object ResponseMapper {
    fun ResponseCharacter.mapToAppDomain(): MarvelCharacter {
        return MarvelCharacter(
            id = id ?: 0,
            name = name.orEmpty(),
            images = thumbnail.toAsset(),
            description = description.orEmpty(),
            comics = comics.toGroupItem(),
            series = series.toGroupItem(),
            stories = stories.toGroupItem()
        )
    }


    private fun Any?.toGroupItem(): GroupItem {
        when (this) {
            is Comics -> {
                val available = this.available != null
                val items = this.items.map { Item(it.name.orEmpty(), it.resourceURI.orEmpty()) }
                return GroupItem(available = available, items = items)
            }
            is Series -> {
                val available = this.available != null
                val items = this.items.map { Item(it.name.orEmpty(), it.resourceURI.orEmpty()) }
                return GroupItem(available = available, items = items)
            }
            is Story -> {
                val available = this.available != null
                val items = this.items.map { Item(it.name.orEmpty(), it.resourceURI.orEmpty()) }
                return GroupItem(available = available, items = items)
            }
            else -> {
                return GroupItem(false, emptyList())
            }
        }
    }

    private fun Thumbnail?.toAsset(): Asset {
        return this?.takeIf { extension != null && path != null }
            ?.let { Asset.fromUrl("$path.$extension") } ?: Asset.NoAsset
    }
}

