package com.troncodroide.app.networkretrofit

import com.troncodroide.app.networkretrofit.interceptor.ApiParamsInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class ClientFactory {
    private val timeOutSeconds = 30L

    fun getRetrofitClientBuilder() : OkHttpClient.Builder {
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }

        return OkHttpClient.Builder()
            .connectTimeout(timeOutSeconds, TimeUnit.SECONDS)
            .readTimeout(timeOutSeconds, TimeUnit.SECONDS)
            .writeTimeout(timeOutSeconds, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor(ApiParamsInterceptor())
    }
}