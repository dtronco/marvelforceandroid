package com.troncodroide.app.networkretrofit.data.datasource

import com.troncodroide.app.core.domain.Pagination
import com.troncodroide.app.data.datasource.CharactersNetworkDataSource
import com.troncodroide.app.domain.MarvelCharacter
import com.troncodroide.app.networkretrofit.ApiProvider
import com.troncodroide.app.networkretrofit.data.datasource.ResponseMapper.mapToAppDomain
import io.reactivex.rxjava3.core.Single

class CharactersRetrofitNetworkDataSource(provider: ApiProvider) : CharactersNetworkDataSource {

    val characterApi = provider.get()

    override fun getCharacters(pagination: Pagination): Single<List<MarvelCharacter>> {
        return characterApi.getCharacters(pagination.offset, pagination.itemsPerPage).map { response ->
            response.data?.results?.map { it.mapToAppDomain() }.orEmpty()
        }
    }

    override fun getCharacter(id: Int): Single<MarvelCharacter> {
        return characterApi.getCharacter(id).map { response ->
            response.data?.results?.first()?.mapToAppDomain()
        }
    }
}