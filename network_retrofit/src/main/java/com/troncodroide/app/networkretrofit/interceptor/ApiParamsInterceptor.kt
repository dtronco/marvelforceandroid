package com.troncodroide.app.networkretrofit.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import java.security.MessageDigest

class ApiParamsInterceptor: Interceptor {


    fun String.md5(): ByteArray = MessageDigest.getInstance("MD5").digest(toByteArray(
        Charsets.UTF_8))
    fun ByteArray.toHex() = joinToString(separator = "") { byte -> "%02x".format(byte) }

    override fun intercept(chain: Interceptor.Chain): Response {
        val apiKey = "bbb1736a9260cf8c68f2e731b38e19c5"
        val privateKey = "198872d5f4f9d94aaba04fa83428d320f483d7d3"
        val ts = System.currentTimeMillis().toString()
        val hash = "$ts$privateKey$apiKey".md5().toHex()
        val apiRequest = chain.request().let { request ->
            val apiUrl = request.url.newBuilder()
                .addQueryParameter("apikey",apiKey)
                .addQueryParameter("ts", ts)
                .addQueryParameter("hash",hash)
                .build()
            request.newBuilder()
                .url(apiUrl)
                .build()
        }
        return chain.proceed(apiRequest)
    }
}