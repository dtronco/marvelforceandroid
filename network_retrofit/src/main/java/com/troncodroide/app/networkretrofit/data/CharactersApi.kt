package com.troncodroide.app.networkretrofit.data

import com.troncodroide.app.networkretrofit.domain.ResponseCharacter
import com.troncodroide.app.networkretrofit.domain.ResponseCharacters
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface CharactersApi {
    @Headers("Cache-Control: no-cache")
    @GET("/v1/public/characters")
    fun getCharacters(
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Single<ResponseCharacters>

    @Headers("Cache-Control: no-cache")
    @GET("/v1/public/characters/{id}")
    fun getCharacter(@Path("id") id: Int): Single<ResponseCharacters>

}