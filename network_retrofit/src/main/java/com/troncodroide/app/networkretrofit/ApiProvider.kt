package com.troncodroide.app.networkretrofit

import com.troncodroide.app.networkretrofit.data.CharactersApi
import com.troncodroide.app.networkretrofit.interceptor.ApiParamsInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class ApiProvider {
    private val timeOutSeconds = 30L


    fun get() : CharactersApi{
        val retrofit = Retrofit.Builder().baseUrl("https://gateway.marvel.com/")
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(getOkHttpClientBuilder().build())
            .build()

        return retrofit.create(CharactersApi::class.java)
    }

    private fun getOkHttpClientBuilder() : OkHttpClient.Builder {
        val logging = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            logging.level = HttpLoggingInterceptor.Level.BODY
        } else {
            logging.level = HttpLoggingInterceptor.Level.NONE
        }

        return OkHttpClient.Builder()
            .connectTimeout(timeOutSeconds, TimeUnit.SECONDS)
            .readTimeout(timeOutSeconds, TimeUnit.SECONDS)
            .writeTimeout(timeOutSeconds, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor(ApiParamsInterceptor())
    }
}
