package com.troncodroide.app.marvelforce.di

import com.troncodroide.app.data.datasource.CharactersCacheDataSource
import com.troncodroide.app.data.datasource.CharactersNetworkDataSource
import com.troncodroide.app.data.repository.CharactersRepository
import com.troncodroide.app.feature_characters.data.datasource.CharactersMemoryCacheDataSource
import com.troncodroide.app.feature_characters.data.repository.CharactersRepositoryImpl
import com.troncodroide.app.feature_characters.data.usecases.GetCharacterUsecase
import com.troncodroide.app.feature_characters.data.usecases.GetCharactersUsecase
import com.troncodroide.app.feature_characters.ui.characters.CharactersListViewModel
import com.troncodroide.app.feature_characters.ui.detail.CharacterDetailViewModel
import com.troncodroide.app.networkretrofit.ApiProvider
import com.troncodroide.app.networkretrofit.data.datasource.CharactersRetrofitNetworkDataSource
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object InjectionModule {
    var appModule = module {

        single { ApiProvider() }
        single { GetCharactersUsecase(get()) }
        single { GetCharacterUsecase(get()) }
        single<CharactersNetworkDataSource> { CharactersRetrofitNetworkDataSource(get()) }
        single<CharactersCacheDataSource> { CharactersMemoryCacheDataSource() }
        single<CharactersRepository> { CharactersRepositoryImpl(get(), get()) }

        viewModel { CharactersListViewModel(get()) }
        viewModel { CharacterDetailViewModel(get(), androidApplication().resources) }
    }
}
