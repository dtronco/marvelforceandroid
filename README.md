# Marvel force
## Android app to consume Marvel api rest

 @author: Daniel Troncoso Meis
 
## Description
Technical application with list and detail of Marvel Characters with the marvel REST api.
Competences: rxJava, kotlin, retrofit, Koin, DataBinding, MVVM 

## Project Schema: 
The preject is developed using MVVM pattern with a separation in layers using clean architecture.
We have this modules:
 - app
 - di
 - data
 - domain
 - feature_x (feature_characters)
 - network_x (network_retrofit)
 - memory_x (No implementations)
 - disk_x (no Implementations)
 - core

### app
The application, here we have the MainActivity that manages all the fragments provided by each feature
the visibility of this module is the core the di and features
This module only knows about contracts and app models. The implementations are hidden and provided by the di module

To manage the views we include the visibility of the feature_x modules (pending to inprove and sahre only to the app a View object that encapsulate the fragment or the activity)

### di
Dependency injection module, its implemented with koin, and this module knows all the implementations of the contracts defined on data and appModels in domain
It is created to hide the implementations/models that are not necesary to app module or other features that only need to know the interface

### data
Data module with the datasource contracts and repositores contract

### domain
App models

### feature_x (feature_characters)
Modules that implements the contract of repository related to this feature
This module also has the ui views related to the feature
We encapsulate all the bussines logic about this feature on this module
Inside this feature we have the character list and the character detail implementations and all the assets realted to this (colors, dimensions, strings, drawables...)

If we need to share elements between features we should create a new module called core_x (core_marvel) and include here all the shared elements like themes, stiles, drawables, dimensions, CustomViews ...

### network_x memory_x, disk_x (network_retrofit)
  On this project we only implement a network varian using retrofit. (network_retrofit)
  On this moduile we implement the contract of characters_network_datasource we have here al the request and response object and network configuration
  
  Exists a memory_cache implementation that should be moved to a module.

### Core
 This module was created to move here the elements, clases, functions, extensions assets that are independendt of the context of the application and are candidates to be moved to an external artifact to use in other projects
 

## Release Notes
### [MFA-1] Starting projcct
- Initial commit with buildSrc
- Add Readme and core module

### [MFA-2] Prepare core 
- Basic interfaces
- Extension file
- Action and Error Core classes
- Add Rx libs

### [MFA-3] Prepare packages on app module
- Create datasources interfaces( only mocks )
- Create repository (marvel)
- Create UseCases
- Create feature module

### [MFA4]
### [MFA5]
### [MFA6]
### [MFA7] 