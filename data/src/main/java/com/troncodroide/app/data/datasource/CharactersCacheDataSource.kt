package com.troncodroide.app.data.datasource

import com.troncodroide.app.core.domain.Pagination
import com.troncodroide.app.domain.MarvelCharacter
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface CharactersCacheDataSource {
    fun getCharacter(id: Int): Single<MarvelCharacter>
    fun hasCharacter(id: Int): Single<Boolean>
    fun hasData(pagination: Pagination): Single<Boolean>
    fun getData(pagination: Pagination): Single<List<MarvelCharacter>>
    fun storeData(pagination: Pagination, data: List<MarvelCharacter>): Completable
    fun clearData(pagination: Pagination): Completable
    fun clearAll(): Completable
}