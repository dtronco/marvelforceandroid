package com.troncodroide.app.data.repository

import com.troncodroide.app.core.domain.Pagination
import com.troncodroide.app.domain.MarvelCharacter
import io.reactivex.rxjava3.core.Single

interface CharactersRepository {

    fun getCharacters(pagination: Pagination): Single<List<MarvelCharacter>>
    fun getCharacter(id: Int): Single<MarvelCharacter>
}