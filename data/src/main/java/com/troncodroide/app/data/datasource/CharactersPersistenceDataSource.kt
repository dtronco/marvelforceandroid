package com.troncodroide.app.data.datasource

import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface CharactersPersistenceDataSource {
    fun getFavorites(): Single<List<Int>>
    fun addFavorite(id: Int): Completable
    fun clearFavorite(id: Int): Completable
}