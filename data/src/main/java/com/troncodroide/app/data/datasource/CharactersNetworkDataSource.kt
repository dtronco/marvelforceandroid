package com.troncodroide.app.data.datasource

import com.troncodroide.app.core.domain.Pagination
import com.troncodroide.app.domain.MarvelCharacter
import io.reactivex.rxjava3.core.Single

interface CharactersNetworkDataSource {
    fun getCharacters(pagination: Pagination): Single<List<MarvelCharacter>>
    fun getCharacter(id: Int): Single<MarvelCharacter>
}